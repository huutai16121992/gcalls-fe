import NavItem from '../NavItem/NavItem';
import './Sidebar.css';

const Sidebar = (props) => {
	return (
		<div className='sidebar'>
			<NavItem icon='fas fa-phone-volume' pathname='/'>
				Call
			</NavItem>
			<NavItem icon='fas fa-history' pathname='/log'>
				History
			</NavItem>
		</div>
	);
};

export default Sidebar;
