import { NavLink } from 'react-router-dom';
import './NavItem.css';

const NavItem = (props) => {
	return (
		<NavLink
			to={props.pathname}
			className={({ isActive }) =>
				`sidebar__items ${
					isActive ? 'sidebar__items--active' : 'sidebar__items--disabled'
				}`
			}>
			<i className={props.icon}></i>
			{props.children}
		</NavLink>
	);
};

export default NavItem;
