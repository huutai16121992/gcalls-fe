import { useEffect } from 'react';
import { useContext, useState } from 'react';
import HistoryItem from '../components/History/HistoryItem';
import { socketContext } from '../store/SocketProvider';
import './CallHistory.css';

const CallHistory = (props) => {
	const socket = useContext(socketContext);
	const [allHistory, setAllHistory] = useState([]);
	const [isDelete, setIsDelete] = useState(false)

	const handleDeleteChange = () => setIsDelete(prevState => !prevState)

	useEffect(() => {
		socket.emit('get');
		socket.on('getAllLog', (allLog) => {
			setAllHistory(allLog);
		});

		return () => {
			socket.off('get');
			socket.off('getAllLog');
		};
	}, [socket, isDelete]);

	const itemHistory = allHistory.map((history) => (
		<HistoryItem
			key={history._id}
			id={history._id}
			status={history.status}
			recipient={history.recipient}
			deleteChanged={handleDeleteChange}
		/>
	));

	return <div className='history'>{itemHistory}</div>;
};

export default CallHistory;
