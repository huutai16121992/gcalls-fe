import JsSIP from 'jssip';

const createNewSocket = () => {
	var socket = new JsSIP.WebSocketInterface('wss://sbc03.tel4vn.com:7444');
	var configuration = {
		sockets: [socket],
		uri: '105@2-test1.gcalls.vn:50061',
		password: 'test1105',
		session_timers: false,
	};

	var ua = new JsSIP.UA(configuration);
	return ua;
};

export default createNewSocket;
