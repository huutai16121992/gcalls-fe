import React, { useState } from 'react';
import io from 'socket.io-client';

export const socketContext = React.createContext({});

const SocketProvider = (props) => {
	const [socket] = useState(
		io('http://huutai.gcalls.vn:8080', {
			port: 8080,
			transports: ['websocket'],
		}),
	);

	return (
		<socketContext.Provider value={socket}>
			{props.children}
		</socketContext.Provider>
	);
};

export default SocketProvider;
